<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlCheckIn extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pl_visit', function($table)
		{
			$table->increments('id');
			$table->unsignedInteger('place_id');
			$table->unsignedInteger('user_id');
			$table->timestamps();
		});

		Schema::table('pl_visit', function($table)
		{
			$table->foreign('place_id')->references('id')->on('pl_places')->onDelete('cascade');
			$table->foreign('user_id')->references('id')->on('pl_users')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pl_visit');
	}

}
