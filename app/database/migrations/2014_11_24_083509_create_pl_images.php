<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlImages extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pl_images', function($table)
		{
			$table->increments('id');
			$table->unsignedInteger('place_id');
			$table->string('image_url', 64);
			$table->timestamps();
		});

		Schema::table('pl_images', function($table)
		{
			$table->foreign('place_id')->references('id')->on('pl_places')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
