<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlPlans extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pl_plan', function($table)
		{
			$table->increments('id');
			$table->unsignedInteger('place_id');
			$table->unsignedInteger('user_id');
			$table->date('plan_date');
			$table->timestamps();
		});

		Schema::table('pl_plan', function($table)
		{
			$table->foreign('place_id')->references('id')->on('pl_places')->onDelete('cascade');
			$table->foreign('user_id')->references('id')->on('pl_users')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pl_plan');
	}

}
