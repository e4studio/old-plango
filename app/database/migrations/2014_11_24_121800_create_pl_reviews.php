<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlReviews extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pl_users', function($table)
		{
			$table->increments('id');
			$table->string('email', 128)->unique()->nullable(false);
			$table->string('username', 32)->default('User')->nullable(false);
			$table->string('password', 60)->nullable(false);
			$table->string('access_token', 100);
			$table->rememberToken();
			$table->timestamps();
		});

		Schema::create('pl_reviews', function($table)
		{
			$table->increments('id');
			$table->unsignedInteger('place_id');
			$table->unsignedInteger('user_id');
			$table->text('review');
			$table->boolean('love');
			$table->timestamps();
		});

		Schema::table('pl_reviews', function($table)
		{
			$table->foreign('place_id')->references('id')->on('pl_places')->onDelete('cascade');
			$table->foreign('user_id')->references('id')->on('pl_users')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
