<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlPlaces extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pl_categories', function($table)
		{
			$table->increments('id');
			$table->string('category_name', 64);
			$table->timestamps();
		});

		Schema::create('pl_places', function($table)
		{
			$table->increments('id');
			$table->string('place_name', 64);
			$table->string('country_code', 2);
			$table->string('city_name', 64);
			$table->string('airport_code', 3);
			$table->unsignedInteger('category_id');
			$table->text('description');
			$table->double('latitude');
			$table->double('longitude');
			$table->timestamps();
		});

		Schema::table('pl_places', function($table)
		{
			$table->foreign('category_id')->references('id')->on('pl_categories')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		
	}

}
