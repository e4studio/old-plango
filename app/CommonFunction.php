<?php

use Illuminate\Support\Collection;  


// require app_path().'/macros.php';

	class CommonFunction
	{
		public static function GenerateError($message, $code)
		{
			$error = new StdClass;
			$error->error = new StdClass;
			$error->error->message = $message;
			$error->error->code = $code;

			return $error;
		}

		public static function GenerateSuccess($success, $code)
		{
			$result = new StdClass;
			$result->result = new StdClass;
			$result->result->message = $success;
			$result->result->code = $code;

			return $result;
		}

		public static function GetUser()
		{
			return User::where('access_token','=',Input::get('token'))->first();
		}

		public static function GetUserID($id)
		{
			return User::find($id);
		}
	}
?>