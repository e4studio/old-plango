<?php

namespace Admin;

/* LARAVEL CLASSES */
use Route;
use DB;
use Auth;
use BaseController;
use Redirect;
use Input;
use Validator;
use View;
use HTML;
use Response;
use Hash;
use StdClass;

/* E4STUDIO CLASSES */
use Place;
use Category;
use Image;
use Review;
use User;


class UserController extends BaseController {

	public $ROUTES = array(
		
		);

	public function get()
	{
		return "GET";
	}

	public function getIndex()
	{
		return "INDEX";
	}

	public function postLogin()
	{
		$credentials = Input::only('email', 'password');
		if(Auth::attempt($credentials, true))
		{
			$response = new StdClass;
			$response->user = Auth::user();
			return json_encode($response);
		}
		else
		{
			$response = new StdClass;
			$response->error = new StdClass;
			$response->error->code = 401;
			$response->error->message = "Incorrect username and password";
			return json_encode($response);
		}
	}

	public function postRegister()
	{
		$input = Input::all();
		$rules = array(
				'email' 		=> 'required|email|unique:pl_users,email|min:3|max:64',
				'password' 		=> 'required|min:3|max:64',
			);

		$validator = Validator::make($input, $rules);
		if($validator->fails())
		{	
			$response = new StdClass;
			$response->error = new StdClass;
			$response->error->code = 406;

			$messages = $validator->messages();
			if ($messages->has('email'))
			{
				$response->error->message = $messages->first('email');    
			}
			else if($messages->has('password'))
			{
				$response->error->message = $messages->first('password');
			}
			
			return json_encode($response);
		}

		$newItem = new User;
		$newItem->email = Input::get('email');
		$newItem->password = Hash::make(Input::get('password'));
		$newItem->access_token = Hash::make($newItem->email . Input::get('password'));
		$newItem->save();

		$response = new StdClass;
		$response->user = $newItem;
		return json_encode($response);
	}
}