<?php

namespace Admin;

/* LARAVEL CLASSES */
use Route;
use DB;
use Auth;
use BaseController;
use Redirect;
use Input;
use Validator;
use View;
use HTML;
use Response;
use Hash;
use StdClass;

/* E4STUDIO CLASSES */
use Place;
use Category;
use Image;
use Review;
use User;
use CommonFunction;


class UserLoggedController extends BaseController {

	public $ROUTES = array(
		
		);

	public function get()
	{
		return "GET";
	}

	public function getIndex()
	{
		return "INDEX";
	}

	public function postUpdate()
	{
		$responseText = "";
		$responseCode = 0;

		if(Input::has('username') && Input::has('email'))
		{
			$user = CommonFunction::GetUser();
			$user->username = Input::get('username');
			$user->email = Input::get('email');
			$user->save();

			$success = CommonFunction::GenerateSuccess("SUCCESS",200);

			$responseText = json_encode($success);
			$responseCode = $success->result->code;
		}
		else
		{
			$error = CommonFunction::GenerateError("Parameter incomplete", 403);

			$responseText = json_encode($error);
			$responseCode = $error->error->code;
		}

		$response = Response::make($responseText, $responseCode);
		$response->header('Content-Type','application/json');
		return $response;
	}
}