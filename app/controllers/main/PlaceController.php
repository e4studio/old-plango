<?php

namespace Main;

/* LARAVEL CLASSES */
use Route;
use DB;
use Auth;
use BaseController;
use Redirect;
use Input;
use Validator;
use View;
use HTML;
use Response;

/* E4STUDIO CLASSES */
use Place;
use Category;
use Image;
use Visit;
use CommonFunction;


class PlaceController extends BaseController {

	public $ROUTES = array(
		'all' 		=> 'places.all',
		'featured' 	=> 'places.featured',
		'add'		=> 'places.add',
		'edit'		=> 'places.edit',
		'image'		=> 'places.image',
		);

	public function get()
	{
		return "GET";
	}

	public function getIndex()
	{
		return "INDEX";
	}

	public function getAll()
	{
		if(Input::has('category'))
			$result = Place::select('pl_places.*','cat.category_name');
		else
			$result = Place::select();

		$categoryFilter = "";

		if(Input::has('id'))
		{
			$result->where('id','=',Input::get('id'));
		}

		if(Input::has('country'))
		{
			$result->where('country_code','=',Input::get('country'));
		}

		if(Input::has('city'))
		{
			$result->where('city_name','=',Input::get('city'));
		}

		if(Input::has('name'))
		{
			$result->where('place_name','LIKE','%' . Input::get('name') . '%');
		}

		if(Input::has('category'))
		{

			$result->join('pl_categories as cat','pl_places.category_id','=','cat.id')
					->where('cat.category_name','=',Input::get('category'));
		}

		$result->with
		(
			array
			(
				'images' => function($query)
				{
					$query->select('place_id','image_url');
				}
			)
		);

		$res = $result->orderBy('place_name')->take(20)->get()->toJSON();
		$response = Response::make($res, 200);
		$response->header('Content-Type','application/json');
		return $response;
	}

	public function getAdd()
	{
		/* VIEW AND PROPERTIES */
		$theView = View::make('main.place.add');
		$theView->routes = $this->ROUTES;

		$theView->categoryList = $this->getCategory();

		return $theView;
	}

	public function getEdit()
	{
		if(Input::has('id') && (($existing = Place::find(Input::get('id'))) != null))
		{
			/* VIEW AND PROPERTIES */
			$theView = View::make('main.place.edit');
			$theView->routes = $this->ROUTES;

			$theView->existing = $existing;

			$theView->categoryList = $this->getCategory($existing->category_id);

			return $theView;
		}
		else
		{
			/* VIEW AND PROPERTIES */
			$theView = View::make('main.place.edit2');
			$theView->routes = $this->ROUTES;

			$theView->categoryList = $this->getCategory();


			return $theView;
		}
	}

	public function postEdit()
	{
		if(Input::has('id') && (($existing = Place::find(Input::get('id'))) != null))
		{
			$existing->place_name = Input::get('place_name');
			$existing->country_code = Input::get('country_code');
			$existing->city_name = Input::get('city_name');
			$existing->airport_code = Input::get('airport_code');
			$existing->category_id = Input::get('category');
			$existing->description = Input::get('description');
			$existing->latitude = Input::get('latitude');
			$existing->longitude = Input::get('longitude');

			$existing->save();


			return Redirect::to(route('places.edit') . "?id=" . $existing->id)->with('message', "Edit succes");
		}
	}

	public function postAdd()
	{
		/* DATA INSERTION */
		$newMachine = New Place;

		$newMachine->place_name = Input::get('place_name');
		$newMachine->country_code = Input::get('country_code');
		$newMachine->city_name = Input::get('city_name');
		$newMachine->airport_code = Input::get('airport_code');
		$newMachine->category_id = Input::get('category');
		$newMachine->description = Input::get('description');
		$newMachine->latitude = Input::get('latitude');
		$newMachine->longitude = Input::get('longitude');

		$newMachine->save();

		$theView = View::make('main.place.add');
		$theView->routes = $this->ROUTES;
		$theView->message = $newMachine->place_name . " added.";

		$theView->categoryList = $this->getCategory();

		return $theView;
	}

	public function postVisit()
	{
		$responseText = "";
		$responseCode = 0;

		$existing = null;

		// Check for the validity of the ID
		if(!Input::has('id')|| ($existing = Place::find(Input::get('id'))) == null)
		{
			$error = CommonFunction::GenerateError("Place not found", 404);

			$responseText = json_encode($error);
			$responseCode = $error->error->code;
		}
		else
		{
			$user = CommonFunction::GetUser();

			$already = Visit::where('place_id','=',Input::get('id'))->where('user_id','=',$user->id)->first();
			if($already == null)
			{
				$newReview = new Visit;
				$newReview->place_id = $existing->id;
				$newReview->user_id = $user->id;

				$newReview->save();
			}

			$result = CommonFunction::GenerateSuccess("SUCCESS",200);
			$responseText = json_encode($result);
			$responseCode = $result->result->code;
		}

		$response = Response::make($responseText, $responseCode);
		$response->header('Content-Type','application/json');
		return $response;
	}

	public function getVisited()
	{
		$responseText = "";
		$responseCode = 0;

		$existing = null;

		// Check for the validity of the ID
		if(!Input::has('id')|| ($existing = CommonFunction::GetUserID(Input::get('id'))) == null)
		{
			$error = CommonFunction::GenerateError("User not found", 404);

			$responseText = json_encode($error);
			$responseCode = $error->error->code;
		}
		else
		{
			$place = Visit::select('place_id')->where('user_id','=',$existing->id)->get();
			$places = array();
			foreach($place as $pl)
			{
				$places[] = $pl->place_id;
			}

			$result = Place::select()->with
			(
				array
				(
					'images' => function($query)
					{
						$query->select('place_id','image_url');
					}
				)
			);

			$result = $result->whereIn('id',$places)->get();

			$responseText = json_encode($result);
			$responseCode = 200;
		}


		$response = Response::make($responseText, $responseCode);
		$response->header('Content-Type','application/json');
		return $response;

	}

	public function getImage()
	{
		if(Input::has('id') && (($existing = Place::find(Input::get('id'))) != null))
		{
			$theView = View::make('main.place.image');
			$theView->routes = $this->ROUTES;
			$theView->existing = $existing;
			
			return $theView;
		}
	}

	public function postImage()
	{
		if(Input::has('id') && (($existing = Place::find(Input::get('id'))) != null))
		{
			$images = Input::get('image');
			$images = explode(',', $images);

			foreach($images as $img)
			{
				$imageNew = new Image;
				$imageNew->place_id = $existing->id;
				$imageNew->image_url = $img;

				$imageNew->save();

				echo $img . " saved. <br/>";
			}


			
			echo "PLEASE CHECK AGAIN IF THE IMAGE INSERTED IS CORRECT (URL and TOTAL)";
		}
	}

	public function getFeatured()
	{
		return "FEATURED";
	}

	private function getCategory($default = '')
	{
		$data = Category::select();
			
			$data->orderBy('category_name');

			return array(
					'default' => $default,
					'list'	  => $this->createArrayFromModel
									(
										$data->get(), 
										'id', 
										'category_name',
										false
									),
			);
	}


	public function createArrayFromModel($list, $value = 'id', $text = 'id', $hasEmpty = true)
		{
			$result = array();

			$list = json_encode($list);
			$list = json_decode($list, true);

			if($hasEmpty)
				$result[] = '&nbsp;';
			foreach($list as $item)
			{	
				$shownText = "";
				if(is_array($text))
				{
					$shownColumn = array();
					foreach($text as $columnName)
					{
						$shownColumn[] = $item[$columnName];
					}
					$shownText = implode(' - ', $shownColumn);
				}
				else
				{
					$shownText = $item[$text];
				}

				$result[$item[$value]] = $shownText;
			}

			return $result;
		}
}