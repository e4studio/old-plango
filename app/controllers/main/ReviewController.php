<?php

namespace Main;

/* LARAVEL CLASSES */
use Route;
use DB;
use Auth;
use BaseController;
use Redirect;
use Input;
use Validator;
use View;
use HTML;
use Response;
use StdClass;

/* E4STUDIO CLASSES */
use Place;
use Category;
use Image;
use Review;
use User;
use CommonFunction;


class ReviewController extends BaseController {

	public $ROUTES = array(
		'all' 		=> 'review.all',
		'add'		=> 'review.add'
		);

	public function get()
	{
		return "GET";
	}

	public function getIndex()
	{
		return "INDEX";
	}

	public function getAll()
	{
		$responseText = "";
		$responseCode = 0;

		$finalResult = "";

		if(Input::has('id'))
		{
			$existing = Place::find(Input::get('id'));
			if($existing == null)
			{
				$error = CommonFunction::GenerateError("Place not found", 404);

				$responseText = json_encode($error);
				$responseCode = $error->error->code;
			}
			else
			{
				$finalResult = Review::where('place_id','=',$existing->id);
				$finalResult->with
				(
					array
					(
						'user' => function($query)
						{
							$query->select('id','username');
						}
					)
				)->orderBy('created_at','DESC');


				$responseText = $finalResult->get()->toJSON();
				$responseCode = 200;
			}
		}
		else
		{
			$error = CommonFunction::GenerateError("ID Not specified", 403);

			$responseText = json_encode($error);
			$responseCode = $error->error->code;
		}

		$response = Response::make($responseText, $responseCode);
		$response->header('Content-Type','application/json');
		return $response;
	}

	public function postAdd()
	{
		$responseText = "";
		$responseCode = 0;

		$existing = null;
		// Check for the validity of the ID
		if(!Input::has('id')|| ($existing = Place::find(Input::get('id'))) == null)
		{
			$error = CommonFunction::GenerateError("Place not found", 404);

			$responseText = json_encode($error);
			$responseCode = $error->error->code;
		}
		else
		{
			$user = CommonFunction::GetUser();
			Review::where('place_id','=',Input::get('id'))->where('user_id','=',$user->id)->delete();

			$newReview = new Review;
			$newReview->place_id = $existing->id;
			$newReview->user_id = $user->id;
			$newReview->review = Input::has('review') ? Input::get('review') : "";
			$newReview->love = Input::has('love');

			$newReview->save();

			$result = CommonFunction::GenerateSuccess("SUCCESS",200);
			$responseText = json_encode($result);
			$responseCode = $result->result->code;
		}

		$response = Response::make($responseText, $responseCode);
		$response->header('Content-Type','application/json');
		return $response;
	}
}