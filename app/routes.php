<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::group
(
	array
	(
		'before' => array()
	),
	function()
	{
		Route::controller('user', 'admin\UserController',
				array
				(
					'postLogin'		=> 	'user.login',
					'postRegister'	=>	'user.register',					
				)
			);
	}
);

Route::group
(
	array
	(
		'before' => array('token.auth')
	),
	function()
	{
		Route::controller('userlog', 'admin\UserLoggedController',
				array
				(
					'postUpdate'		=> 	'user.update'
				)
			);
	}
);

Route::group
(
	array
	(
		'before' => array('token.auth')
	),
	function()
	{

		Route::controller('places', 'main\PlaceController',
				array
				(
					'getAll'		=>	'places.all',
					'getFeatured'	=> 	'places.featured',
					'getAdd'		=>  'places.add',
					'postAdd'		=>	'places.add',
					'postVisit'		=>	'places.visit',
					'getEdit'		=>	'places.edit',
					'getImage'		=>	'places.image',
					'getVisited'	=>	'places.visit',
				)
			);

		Route::controller('review', 'main\ReviewController',
				array
				(
					'get'			=>	'places.root',
					'getIndex'		=>	'places.getIndex',
					'getAll'		=>	'places.all',
				)
			);
	}
);
