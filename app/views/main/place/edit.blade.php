<form name="addNewUser" method="POST" action="{{ route($routes['edit']) }}" >
		<div class="row">

			{{ (isset($message) ? $message : "" )}}

			<input type='hidden' name='id' value='{{$existing->id}}'/>

			{{-- User Group --}}
			{{ $errors->first('place_name', '<span style=\'color:red\'>:message</span><br/>') }}
			{{ Form::label('place_name', "Name" ) }}
					<input 	name="place_name" 
					value="{{ $existing->place_name }}"/>
			
			<br/>	

			{{ $errors->first('country_code', '<span style=\'color:red\'>:message</span><br/>') }}
			{{ Form::label('country_code', "Country Code" ) }}
					<input 	name="country_code" placeholder="2 letters all capitals"
					value="{{ $existing->country_code }}"/>

			<br/>

			{{ $errors->first('city_name', '<span style=\'color:red\'>:message</span><br/>') }}
			{{ Form::label('city_name', "City Name" ) }}
					<input 	name="city_name"
					value="{{ $existing->city_name }}"/>

			<br/>

			{{ $errors->first('airport_code', '<span style=\'color:red\'>:message</span><br/>') }}
			{{ Form::label('airport_code', "Airport Code" ) }}
					<input 	name="airport_code" placeholder="3 letters. All capitals"
					value="{{ $existing->airport_code }}"/>

			<br/>

			<?php $category = Category::find($existing->category_id) ?>


			{{ $errors->first('category', '<span style=\'color:red\'>:message</span><br/>') }}
			{{ Form::label('category', "Category" ) }}


			{{ Form::select('category', $categoryList['list'], $categoryList['default']) }}
			<br/> 

			{{ $errors->first('description', '<span style=\'color:red\'>:message</span><br/>') }}
			{{ Form::label('description', "Description" ) }}
			<br/>
			{{ Form::textarea('description', $existing->description)}}
			<br/>

			{{ $errors->first('latitude', '<span style=\'color:red\'>:message</span><br/>') }}
			{{ Form::label('latitude', "latitude" ) }}
			<input 	name="latitude" placeholder="latitude"
					value="{{ $existing->latitude }}"/>
			<br/>

			{{ $errors->first('longitude', '<span style=\'color:red\'>:message</span><br/>') }}
			{{ Form::label('longitude', "longitude" ) }}
			<input 	name="longitude" placeholder="longitude"
					value="{{ $existing->longitude }}"/>
			<br/>

			{{ Form::submit('save') }}

		</div>
	</form>