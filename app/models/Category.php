<?php
	
	class Category extends Eloquent
	{
		public $table = "pl_categories";

		public function places()
		{
			return $this->hasMany('Place', 'category_id');
		}
	}
?>