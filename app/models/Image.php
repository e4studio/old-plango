<?php
	
	class Image extends Eloquent
	{
		public $table = "pl_images";

		public function place()
		{
			return $this->belongsTo('Place', 'place_id');
		}
	}
?>