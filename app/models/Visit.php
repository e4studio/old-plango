<?php
	
	class Visit extends Eloquent
	{
		public $table = "pl_visit";

		public function places()
		{
			return $this->belongsTo('Place', 'place_id');
		}

		public function user()
		{
			return $this->belongsTo('User','user_id');
		}
	}
?>