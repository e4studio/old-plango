<?php
	
	class Place extends Eloquent
	{
		public $table = "pl_places";
		protected $appends = array('total_love');

		public function category()
		{
			return $this->belongsTo('Category','category_id');
		}

		public function images()
		{
			return $this->hasMany('Image', 'place_id');
		}

		public function imagesList()
		{
			return $this->hasMany('Image', 'place_id');
		}

		public function getTotalLoveAttribute()
		{
			return Review::where('place_id','=',$this->id)
							->where('love','=',1)->get()->count();
		}
	}
?>